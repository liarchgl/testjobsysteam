﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class TestJobs : MonoBehaviour
{
    private bool _startJob = false;
    private List<JobHandle> _jobHandles;
    private float _startTime;
    private bool _complete = false;
    private int _frameNumber = 0;

    private List<NativeArray<Thing>> _thingss;
    // Start is called before the first frame update
    void Start()
    {
        _jobHandles = new List<JobHandle>();
        
        _thingss = new List<NativeArray<Thing>>();
        for (int i = 0; i < 1; ++i)
        {
            List<Thing> things = new List<Thing>();
            for (int j = 0; j < 1000; ++j)
            {
                things.Add(new Thing(Random.Range(0, 100)));
            }
            _thingss.Add(new NativeArray<Thing>(things.ToArray(), Allocator.Persistent));
        }

        foreach (var things in _thingss)
        {
            JobHandle jobHandle = new SquareNumbers(things).Schedule(things.Length, 12);
//            JobHandle jobHandle = new SquareNumbers(things).Schedule();
            _jobHandles.Add(jobHandle);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_frameNumber == 0)
        {
        }
        else if (_frameNumber == 1)
        {
            _startTime = Time.realtimeSinceStartup;
            print($"Size:{_jobHandles.Count}");
            RunJobs();
//            foreach (var jobHandle in _jobHandles)
//            {
//                if (!jobHandle.IsCompleted)
//                {
//                    print($"{jobHandle.ToString()} is not complete!");
//                }
//            }
        }
        else if (_frameNumber == 2)
        {
            print($"Cost {Time.realtimeSinceStartup - _startTime}s!");
            
            foreach (var things in _thingss)
            {
                things.Dispose();
            }
        }

        if (_frameNumber < 100000000)
        {
            ++_frameNumber;
        }
    }

    void RunJobs()
    {
        foreach (var jobHandle in _jobHandles)
        {
            jobHandle.Complete();
        }
    }
}
