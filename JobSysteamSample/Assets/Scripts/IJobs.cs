﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public struct Thing
{
    public Thing(float value)
    {
        this._value = value;
    }
    private float _value;

    public void Square()
    {
        _value *= _value;
    }

    public void Prescribe()
    {
        _value = Mathf.Pow(_value, 0.5f);
    }
}

public struct SquareNumbers : IJobParallelFor
{
    public NativeArray<Thing> Things;

    public SquareNumbers(NativeArray<Thing> things)
    {
        Things = things;
    }
    
    public void Execute()
    {
        foreach (var thing in Things)
        {
            for (int i = 0; i < 1000; ++i)
            {
                thing.Square();
            
                thing.Prescribe();
            }
        }
    }

    public void Execute(int index)
    {
        var thing = Things[index];

        for (int i = 0; i < 1000000; ++i)
        {
            thing.Square();
        
            thing.Prescribe();
        }
    }
}
